import model.Student;
import model.Students;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ReadCsv {

    private static final String COMMA_DELIMITER = ",";

    private String fileName = "persons.csv";

    private Students students = new Students();

    public void readCsv() throws IOException {

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(COMMA_DELIMITER);
                List<String> replaced = Arrays.stream(values).map(n -> n.replace("\"", "")).collect(Collectors.toList());
                students.addStudent(new Student(replaced.get(1), replaced.get(2)));
            }
        }
    }

    public Students getStudents() {
        return students;
    }

    public void setStudents(Students students) {
        this.students = students;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

}
