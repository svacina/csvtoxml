package model;

public class Student {

    private static Integer idCounter = 1;
    private static String modifiedTimeConst="2019-03-31T16:11:06+01:00";
    private static String localContactConst="1";

    private String id;
    private String name;

    public Student(String first, String last){
        this.name = first + " " + last;
        this.id = idCounter.toString();
        idCounter += 1;
    }


    // <contact id="8e4be892811e1965" name="Katie Allison" modified_time="2019-03-31T16:11:06+01:00" local_contact="1"/>
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("<contact id=\"");
        builder.append(id);
        builder.append("\" ");
        builder.append("name=\"");
        builder.append(name);
        builder.append("\" ");
        builder.append("modified_time=\"2019-03-31T16:11:06+01:00\" local_contact=\"1\"/>");
        builder.append("\n");
        return builder.toString();
    }

}
