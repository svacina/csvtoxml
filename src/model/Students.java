package model;


import java.util.ArrayList;
import java.util.List;

public class Students {

    private List<Student> students = new ArrayList<>();

    public void addStudent(Student student){
        students.add(student);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<contacts>\n");
        Student s;
        for (int i = 1; i < students.size(); i++) {
            s = students.get(i);
            stringBuilder.append(s.toString());
        }
        stringBuilder.append("</contacts>");
        return stringBuilder.toString();
    }
}
