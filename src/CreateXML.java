import model.Students;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CreateXML {

    private static FileOutputStream out;

    public static void createFile(){

        try {
            out = new FileOutputStream("/Users/icpc/Documents/contacts.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void writeToFile(Students students){
        try {
            out.write(students.toString().getBytes());
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
