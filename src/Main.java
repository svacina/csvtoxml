import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        ReadCsv readCsv = new ReadCsv();
        try {
            readCsv.readCsv();
        } catch (IOException e) {
            e.printStackTrace();
        }

        CreateXML.createFile();
        CreateXML.writeToFile(readCsv.getStudents());

    }
}
